package tests;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import io.restassured.http.ContentType;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

import java.util.HashMap;
import java.util.Map;

public class PutPatchDeleteTests {

	
	@Test
	public void testPut() {
		baseURI = "https://reqres.in/api";

		JSONObject request = new JSONObject();
		request.put("name", "Lim");
		request.put("job", "QA Developer");
		
		System.out.println(request.toJSONString());
		
		given()
		.header("Content-Type", "application/json")
		.contentType(ContentType.JSON)// sending type json
		.accept(ContentType.JSON) // only accepting json
		.body(request.toJSONString())
		.when().put("/users/2")
		.then().statusCode(200)
		.log().all();
	}
	
	@Test
	public void testPatch() {
		baseURI = "https://reqres.in/api";

		JSONObject request = new JSONObject();
		request.put("name", "Lim");
		request.put("job", "QA Developer");
		
		System.out.println(request.toJSONString());
		
		given()
		.header("Content-Type", "application/json")
		.contentType(ContentType.JSON)// sending type json
		.accept(ContentType.JSON) // only accepting json
		.body(request.toJSONString())
		.when().patch("/users/2")
		.then().statusCode(200)
		.log().all();
	}
	
	@Test
	public void testDelete() {
		baseURI = "https://reqres.in/api";

		when().delete("/users/2")
		.then().statusCode(204)
		.log().all();
	}
}
