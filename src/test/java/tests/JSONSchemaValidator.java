package tests;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;


import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;

import org.testng.annotations.Test;

public class JSONSchemaValidator {

	@Test
	public void testValidation() {

		baseURI = "https://reqres.in/api";

		given()
		.get("/users?page=2")
		.then()
		.assertThat()
		.body(matchesJsonSchemaInClasspath("schema.json"))
		.statusCode(200);
	}
	
	@Test
	public void testLocalValidation() {

		baseURI = "http://localhost:3000";

		given()
		.get("/users")
		.then()
		.assertThat()
		.body(matchesJsonSchemaInClasspath("local_schema.json"))
		.statusCode(200);
	}
}
