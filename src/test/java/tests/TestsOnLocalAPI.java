package tests;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import io.restassured.http.ContentType;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

import java.util.HashMap;
import java.util.Map;

public class TestsOnLocalAPI {

	@Test(description = "GET Request /users")
	public void get() {

		baseURI = "http://localhost:3000";

		given().get("/users").then().statusCode(200).log().all();
	}

	@Test(description = "POST Request /users")
	public void post() {
		JSONObject request = new JSONObject();

		request.put("firstName", "John");
		request.put("lastName", "Doe");
		request.put("subjectId", 1);

		baseURI = "http://localhost:3000";

		given().contentType(ContentType.JSON).accept(ContentType.JSON).body(request.toJSONString()).when()
				.post("/users").then().statusCode(201);
	}

	@Test(description = "PUT Request /users")
	public void put() {
		JSONObject request = new JSONObject();

		request.put("firstName", "Jane");
		request.put("lastName", "Doe");
		request.put("subjectId", 2);

		baseURI = "http://localhost:3000";

		given().contentType(ContentType.JSON).accept(ContentType.JSON).body(request.toJSONString()).when()
				.put("/users/4").then().statusCode(200).log().all();
	}

	@Test(description = "PATCH Request /users")
	public void patch() {
		JSONObject request = new JSONObject();

		request.put("lastName", "Fake Last Name");

		baseURI = "http://localhost:3000";

		given().contentType(ContentType.JSON).accept(ContentType.JSON).body(request.toJSONString()).when()
				.patch("/users/4").then().statusCode(200).log().all();
	}

	@Test(description = "DELETE Request /users")
	public void delete() {

		baseURI = "http://localhost:3000";

		when().put("/users/4").then().statusCode(200).log().all();
	}
}
