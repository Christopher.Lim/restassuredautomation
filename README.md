# RESTAssuredAutomation

This project uses apache maven, rest assured, testng json-simple from google, json schema validator and commons io.
And this shows basic fundamentals on how to write REST API test automation.
</br>

We are also going to be using json-server to run our local db for testing purposes.

### Installation
Install JSON Server as global in your machine.
```
npm install -g json-server
```
Create a db.json file in your root folder with some data.
```json
{
	"users": [
		{
			"firstName": "User",
			"lastName": "1",
			"subjectId": 1,
			"id": 1
		},
		{
			"firstName": "User",
			"lastName": "2",
			"subjectId": 2,
			"id": 2
		},
		{
			"firstName": "User",
			"lastName": "3",
			"subjectId": 1,
			"id": 3
		}
	],
	"subjects": [
		{
			"name": "Automation",
			"id": 1
		},
		{
			"name": "Engineer",
			"id": 2
		},
		{
			"name": "Student",
			"id": 3
		}
	]
}

```
Start the JSON Server
```
json-server --watch db.json
```
Now if you go over to `localhost:3000` you will something similar to this.
</br>
![Alt text](src/main/resources/json-server.png "JSON Server")
</br></br>
Then run the `TestOnLocalAPI` to test the `db.json`.
</br>
### Credits
This repo is made possible by taking the video course of Raghav Pal from https://automationstepbystep.com/ .